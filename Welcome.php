<head>
<style>
body {
	background-image: url("bgimage.jpg");
	background-color: #cccccc;
	background-position: center;
	background-size: contain;
	background-repeat: no-repeat;
	height: 1000px;
}

.DivBox {
	background-color: white;
	display: block;
	margin-left: auto;
	margin-right: auto;
	width: 40%;
	text-align: center;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<br/>
<br/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<div class="DivBox" >
<?php
echo 'Hi ' . htmlspecialchars($_POST["firstname"]) . ',';
?>

 <br/> <br/>
	Welcome! <br/>
    Don't be left in the dark... <br/><br/>
    You should recieve an e-mail with a link shortly to verify your address, once you click the link we will start creating your system, this process will take a few minutes. <br/>
	Please take a brief moment to watch this short video while we create your custom system.  This way you will know exactly what to expect next. <br/><br/>
    <b>Initially, getting your new system setup is generally easier using a desktop or laptop computer.</b> <br/>
	</div>
<div class="DivBox" >
    After the initial setup, access from your smartphone is simple using the same link to your system from your phone. <br/><br/>
    <center>Welcome to OfficeSweeet <br/> <iframe width="560" height="315" src="https://www.youtube.com/embed/HfzpDgBtdPo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> <br/>
	Please watch this short video.</a></center> <br/><br/>
	
</div>
<div class="DivBox" >
    In this video you will learn;<br/>
    1.	What you expect going forward.<br/>
    2.	How to access your new system.<br/>
    3.	What OfficeSweeet looks like on the inside.<br/>
    4.	How to reach out to us to simplify your experience. <br/><br/>
    Regards, <br/><br/>John Stagl <a href="mailto:jstagl@officesweeet.com">jstagl@officesweeet.com</a><br/>813.444.5284 x1690<br/>Main St., #786<br/>Safety Harbor, FL, 34695<br/> USA <br/>
	</center>
</div>